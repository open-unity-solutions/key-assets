#if ODIN_INSPECTOR_3_1 && UNITY_EDITOR

using Sirenix.OdinInspector.Editor.Validation;
using Valhalla.Serialization.Editor;


[assembly: RegisterValidator(typeof(KeyAssetValidator))]
namespace Valhalla.Serialization.Editor
{
	
	public class KeyAssetValidator : RootObjectValidator<KeyAsset>
	{
		protected override void Validate(ValidationResult result)
		{
			if (string.IsNullOrEmpty(Value.Key))
				result
					.AddError("Empty key")
					.WithFix("Set file name", () => Value.SetNameAsKey());
		}
		
	}
}

#endif