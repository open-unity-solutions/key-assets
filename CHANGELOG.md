## [1.2.4] – 2023-07-11

### Added
- Methods for dictionaries with `IKeyAsset`s

### Removed
- Hash algorithm


## [1.2.3] – 2023-07-05

### Fixed
- Hash algorithm fixed


## [1.2.2] – 2023-07-05

### Changed
- `KeyAsset` compare logic based on keys
- CI/CD updated for modularity


## [1.2.1] – 2023-06-16

### Fixed
- `SerializedKeyAsset` now validated


## [1.2.0] – 2023-06-16

### Added
- `SerializedKeyAsset`, used with Odin, added
- Interface with generic methods created


## [1.1.0]

### Changed
- Rebranded to 'Valhalla'


## [1.0.3]

### Added
- Editor setter 


## [1.0.2]

### Fixed
- Assemblies fixed


## [1.0.1]

### Added
- Validation via Odin Validator


## [1.0.0]

### Added
- `KeyAsset` and `IKeyAsset`
