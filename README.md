### About

`ScriptableObjects` with Key field. And some interfaces, if you can't use inheritance.

Used mostly for JSON serialization.
