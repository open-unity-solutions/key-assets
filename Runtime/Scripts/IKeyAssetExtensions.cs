using UnityEditor;
using UnityEngine;


namespace Valhalla.Serialization
{
	// ReSharper disable once InconsistentNaming
	public static class IKeyAssetExtensions
	{
		#if UNITY_EDITOR
		
		
		public static void SetNameAsKey<TKeyAsset>(this TKeyAsset keyAsset)
			where TKeyAsset : ScriptableObject, IKeyAsset
		{
			keyAsset.Key = keyAsset.GetNameForKey();
			keyAsset.SaveAssetToEditorDatabase();
		}
		
		
		public static void SaveAssetToEditorDatabase<TKeyAsset>(this TKeyAsset keyAsset)
			where TKeyAsset : ScriptableObject, IKeyAsset
		{
			EditorUtility.SetDirty(keyAsset);
			AssetDatabase.SaveAssets();
		}


		
		#endif
	}
}
