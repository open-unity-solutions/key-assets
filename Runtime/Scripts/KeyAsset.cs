using System;
using UnityEngine;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif

#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Valhalla.Serialization
{
	/// <summary>
	///     Asset, that can be serialized to JSON with string-based key.
	/// </summary>
	public class KeyAsset : ScriptableObject, IKeyAsset, IEquatable<IKeyAsset>
	{
		/// <summary>
		///     Key of asset. Must be unique for it's type.
		/// </summary>
		#if ODIN_INSPECTOR && UNITY_EDITOR
		[FoldoutGroup("Key", true)]
		[LabelWidth(50)]
		[InlineButton(nameof(OnEditorFixButtonClick), "Insert file name")]
		#endif
		[SerializeField]
		private string _key;


		/// <summary>
		///     Key field, must be set via <see cref="_key" /> in inspector.
		/// </summary>
		public string Key
		{
			get => _key;
			
			#if UNITY_EDITOR
			set => _key = value;
			#endif
		}

		
		public string GetNameForKey()
			=> name;


		public bool Equals(KeyAsset other)
			=> Equals(other as IKeyAsset);


		public bool Equals(IKeyAsset other)
		{
			if (ReferenceEquals(null, other))
				return false;

			if (ReferenceEquals(this, other))
				return true;

			return base.Equals(other) && Key == other.Key;
		}


		public static bool operator ==(KeyAsset left, KeyAsset right) 
			=> Equals(left, right);


		public static bool operator !=(KeyAsset left, KeyAsset right) 
			=> !Equals(left, right);


		#if UNITY_EDITOR
		
		private void OnEditorFixButtonClick()
			=> this.SetNameAsKey();
		
		#endif
	}
}
