using System;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Valhalla.Serialization
{
	public class SerializedKeyAsset : SerializedScriptableObject, IKeyAsset, IEquatable<IKeyAsset>
	{
		/// <summary>
		///     Key of asset. Must be unique for it's type.
		/// </summary>
		#if UNITY_EDITOR
		[FoldoutGroup("Key", true)]
		[LabelWidth(50)]
		[InlineButton(nameof(OnEditorFixButtonClick), "Insert file name")]
		#endif
		[SerializeField]
		private string _key;


		/// <summary>
		///     Key field, must be set via <see cref="_key" /> in inspector.
		/// </summary>
		public string Key
		{
			get => _key;
			
			#if UNITY_EDITOR
			set => _key = value;
			#endif
		}

		
		public string GetNameForKey()
			=> name;
		
		
		public override bool Equals(object other) 
			=> base.Equals(other);
		
		
		public bool Equals(KeyAsset other)
			=> Equals(other as IKeyAsset);


		public bool Equals(IKeyAsset other)
		{
			if (ReferenceEquals(null, other))
				return false;

			if (ReferenceEquals(this, other))
				return true;

			return base.Equals(other) && Key == other.Key;
		}


		public override int GetHashCode() 
			=> HashCode.Combine(base.GetHashCode(), Key);


		public static bool operator ==(SerializedKeyAsset left, KeyAsset right) 
			=> Equals(left, right);


		public static bool operator !=(SerializedKeyAsset left, KeyAsset right) 
			=> !Equals(left, right);


		#if UNITY_EDITOR
		
		private void OnEditorFixButtonClick()
			=> this.SetNameAsKey();
		
		#endif
	}
}
