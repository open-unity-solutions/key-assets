﻿namespace Valhalla.Serialization
{
	/// <summary>
	///     Guarantees, that type can be serialized as string key
	/// </summary>
	public interface IKeyAsset
	{
		/// <summary>
		///     Key for JSON serialization
		/// </summary>
		string Key
		{
			get;
			
			#if UNITY_EDITOR
			set;
			#endif

		}
		
		#if UNITY_EDITOR
		string GetNameForKey();
		#endif
	}
}
