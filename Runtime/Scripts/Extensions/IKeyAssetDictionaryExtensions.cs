using System;
using System.Collections.Generic;
using System.Linq;


namespace Valhalla.Serialization.Extensions
{
	// ReSharper disable once InconsistentNaming
	public static class IKeyAssetDictionaryExtensions
	{
		public static TValue GetValue_KeyProtected<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
			where TKey : IKeyAsset
			=> dictionary.GetValue_KeyProtected(key.Key);
		
		public static TValue GetValue_KeyProtected<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, string key)
			where TKey : IKeyAsset
			=> dictionary[dictionary.GetKeyAsset_KeyProtected(key)];
		
		
		public static void Add_KeyProtected<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey keyAsset, TValue value)
			where TKey : IKeyAsset
		{
			if (dictionary.ContainsKey_KeyProtected(keyAsset))
				throw new ArgumentException("An element with the same key already exists in the dictionary.");
			
			dictionary.Add(keyAsset, value);
		}
		
		
		public static bool ContainsKey_KeyProtected<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey keyAsset)
			where TKey : IKeyAsset
		{
			if (dictionary.ContainsKey(keyAsset))
				return true;
			
			return dictionary.ContainsKey_KeyProtected(keyAsset.Key);
		}


		public static bool ContainsKey_KeyProtected<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, string key)
			where TKey : IKeyAsset
			=> dictionary.Keys.Any(entry => entry.Key == key);


		public static TKey GetKeyAsset_KeyProtected<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
			where TKey : IKeyAsset
			=> dictionary.GetKeyAsset_KeyProtected(key.Key);
		
		
		public static TKey GetKeyAsset_KeyProtected<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, string key)
			where TKey : IKeyAsset
			=> dictionary.Keys.First(entry => entry.Key == key);


		public static bool Remove_KeyProtected<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
			where TKey : IKeyAsset
			=> dictionary.Remove_KeyProtected(key.Key);
		
		public static bool Remove_KeyProtected<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, string key)
			where TKey : IKeyAsset
		{
			if (!dictionary.ContainsKey_KeyProtected(key))
				return false;
			
			var keyAsset = dictionary.GetKeyAsset_KeyProtected(key);
			return dictionary.Remove(keyAsset);
		}
	}
}
